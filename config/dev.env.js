'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  DOMAIN: '"turelo-dev.auth.us-east-1.amazoncognito.com"',
  CLIENTID: '"239phm74fsbqqlduvgn08bn1hs"'
})
