/* eslint-disable */
import axios from 'axios';

const BASE_URL = 'https://8fft56lncg.execute-api.us-east-1.amazonaws.com/dev';

export function getCategories() {
  const url = `${BASE_URL}/categories`;
  return axios.get(url, { headers: { Authorization: `Bearer ${localStorage.getItem('access_token')}` }}).then(response => response.data.catagories).catch(err => err || 'Unable to retrieve data');
}

export function getCategory(id) {
  const url = `${BASE_URL}/category/` + id;
  return axios.get(url, { headers: { Authorization: `Bearer ${localStorage.getItem('access_token')}` }}).then(response => response.data).catch(err => err || 'Unable to retrieve data');
}

export function newCategory(form) {
  const url = `${BASE_URL}/category`;
  console.log("the payload:". form)
  return axios.post(url, form, { headers: { Authorization: `Bearer ${localStorage.getItem('access_token')}` }}).then(response => response.data).catch(err => err || 'Unable to post data');
}