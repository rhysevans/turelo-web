/* eslint-disable */
import axios from 'axios';

const BASE_URL = 'https://8fft56lncg.execute-api.us-east-1.amazonaws.com/dev';

export function getFeeds() {
  const url = `${BASE_URL}/feeds`;
  return axios.get(url, { headers: { Authorization: `Bearer ${localStorage.getItem('access_token')}` }}).then(response => response.data).catch(err => err || 'Unable to retrieve data');
}

export function getFeed(id) {
  const url = `${BASE_URL}/feed/` + id;
  return axios.get(url, { headers: { Authorization: `Bearer ${localStorage.getItem('access_token')}` }}).then(response => response.data).catch(err => err || 'Unable to retrieve data');
}

export function getFeedEntry(id) {
    const url = `${BASE_URL}/feedEntry/` + id;
    return axios.get(url, { headers: { Authorization: `Bearer ${localStorage.getItem('access_token')}` }}).then(response => response.data).catch(err => err || 'Unable to retrieve data');
}

export function getFeedEntries(id) {
  const url = `${BASE_URL}/feedEntries/` + id;
  return axios.get(url, { headers: { Authorization: `Bearer ${localStorage.getItem('access_token')}` }}).then(response => response.data).catch(err => err || 'Unable to retrieve data');
}