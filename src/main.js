// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';
import VueNoty from 'vuejs-noty';
import App from './App';
import router from './router';
import store from './store/index.js';

var VueTruncate = require('vue-truncate-filter')

Vue.config.productionTip = false;

Vue.use(BootstrapVue);
Vue.use(VueNoty);
Vue.use(VueTruncate);
Vue.use(require('vue-moment'));

import 'vuejs-noty/dist/vuejs-noty.css';

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App },
});
