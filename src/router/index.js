import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/components/Home';
import Landing from '@/components/Landing';
import Feeds from '@/components/Feeds';
import FeedVue from '@/components/FeedView';
import FeedEntry from '@/components/FeedEntry';
import Discover from '@/components/Discover';
import Categories from '@/components/Categories';
import CategoryView from '@/components/CategoryView';
import Profile from '@/components/Profile';
import Callback from '@/components/Callback';
import ErrorMsg from '@/components/ErrorMsg';
import store from '../store';


Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    { path: '/landing', name: 'landing', meta: { title: 'turelo', auth: false }, component: Landing },
    { path: '/callback', meta: { title: 'Authenticating...', auth: false }, component: Callback },
    { path: '/', name: 'home', meta: { title: 'Home', auth: true }, component: Home,
    children: [
    { path: '/feeds', name: 'feeds', meta: { title: 'Feeds', auth: true }, component: Feeds },
    { path: '/error', name: 'error', meta: { title: 'Error', auth: false }, component: ErrorMsg },
    { path: '/profile', name: 'profile', meta: { title: 'Profile', auth: true }, component: Profile},
    { path: '/feeds/:id', name: 'FeedView', meta: { title: 'Feed', auth: true }, component: FeedVue },
    { path: '/feedEntry/:id', name: 'FeedEntry', meta: { title: 'FeedEntry', auth: true }, component: FeedEntry },
    { path: '/discovery', name: 'Discovery', meta: { title: 'Discover', auth: true }, component: Discover },
    { path: '/categories', name: 'Categories', meta: { title: 'Categories', auth: true }, component: Categories },
    { path: '/categories/:id', name: 'CategoryView', meta: { title: 'Category', auth: true }, component: CategoryView },
    ]}
  ],
});

router.beforeEach((to, from, next) => {
  // Use the page's router title to name the page
  if (to.meta && to.meta.title) {
    document.title = to.meta.title;
  }

  // Redirect to the login page if not authenticated
  // for pages that have 'auth: true' set
  if (to.meta && to.meta.auth !== undefined) {
    if (to.meta.auth) {
      if (store.getters.isAuthenticated) {
        next()
      } else {
        router.push({ name: 'landing' });
      }
    } else {
      if (store.getters.isAuthenticated) {
        router.push({ name: 'landing' });
      } else {
        next()
      }
    }
  } else {
    next()
  }
})

router.beforeResolve((to, from, next) => {
  // If this isn't an initial page load.
  if (to.name) {
      // Start the route progress bar.
      NProgress.start()
  }
  next()
})

router.afterEach((to, from) => {
  // Complete the animation of the route progress bar.
  NProgress.done()
})

export default router