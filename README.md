# turelo web client

> This is the web front end for the turelo RSS reader

## Configuration

This project doesnt use any third party SDK's to manage the cognito auth, have a look in auth/auth.js and store/auth.js to see how this application deals with the auth flow and stores the result from the callback.

To configure this project for your own cognito environmnet please alter the variables in the config/{dev/prod} file as required

```
module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  DOMAIN: '"xxxxxxx"',
  CLIENTID: '"xxxxxxx"'
})
```

## Building the project

``` bash
# install dependencies
yarn install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

## To be done

TBD